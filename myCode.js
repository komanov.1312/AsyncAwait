// Пример 1
// fetch('https://api.github.com/users/dk-1312')
//   .then((res) => {
//     return res.json();
//   })
//   .then((res) => {
//     console.log(res);
//   })
//   .catch((err) => {
//     console.log(err);
//   });

// Тоже самое с async/await
const getGitData = async () => {
  try {
    const response = await fetch('https://api.github.com/users/dk-1312');
    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.log('Сбой получения данных: ', error);
  }
};

getGitData();

// Пример 2 "Получение видеопотока с вебкамеры в браузере"
// const video = document.querySelector('video');

// const myVideo = navigator.mediaDevices
//   .getUserMedia({ video: true })
//   .then((mediaStream) => {
//     video.srcObject = mediaStream;
//   })
//   .catch((err) => {
//     console.log('video error: ', err);
//   });
// console.log(myVideo);

// Тоже самое с async/await
const video = document.querySelector('video');
const getUserVideo = async () => {
  try {
    const response = await navigator.mediaDevices.getUserMedia({ video: true });
    video.srcObject = response;
  } catch (error) {
    console.log('Ошибка получения данных с веб-камеры >>>', error);
  }
};
getUserVideo();

// Пример 3.
function sleep(time) {
  return new Promise((resolve, reject) => {
    if (time < 500) {
      reject('Слишком мало сна');
    }
    setTimeout(() => resolve(`Поспал ${time}`), time);
  });
}

const sleepSession = async () => {
  try {
    const sleep1 = await sleep(1500);
    console.log(sleep1);
    const sleep2 = await sleep(1000);
    console.log(sleep2);
    const sleep3 = await sleep(500);
    console.log(sleep3);
    const sleep4 = await sleep(200);
    console.log(sleep4);
  } catch (error) {
    console.log(error);
  }
};
sleepSession();

// sleep(1500)
//   .then((res) => {
//     console.log(res);
//     return sleep(1000);
//   })
//   .then((res) => {
//     console.log(res);
//     return sleep(500);
//   })
//   .then((res) => {
//     console.log(res);
//     return sleep(200);
//   })
//   .then((res) => {
//     console.log(res);
//     return sleep(100);
//   })
//   .catch((err) => {
//     console.log('Ошибка!!!!', err);
//   });
